---
title: "Shared Code"
subtitle: "of my DDJ master thesis in Political Science @ UZH"
author: "Salim Brüggemann (salim@posteo.ch)"
date: "`r Sys.Date()`"
output: html_document
---

### Shared options

#### Setup

_Remember to adjust the working directory below to your personal setup before running any of the scripts!_

```{r setup, comment = NA, results = 'hide', cache = FALSE}
# set working directory
setwd(dir = "~/Dokumente/Ausbildung/Studium UZH/Master/Masterarbeit/GitLab/ddj_master_thesis_code")

# set maximum width of printed strings in number of characters
string_width <- 123

# set global R options
options(digits = 5,
        scipen = 999,
        tibble.width = string_width,
        google_font.work_sans = "https://fonts.googleapis.com/css?family=Work+Sans:200,300,400,600,700")

# set global knitr options
knitr::opts_chunk$set(comment = NA,
                      results = 'hide',
                      cache = TRUE,
                      autodep = TRUE,
                      cache.extra = list(R.version,
                                         sessionInfo()),
                      cache.path = ".knitr_cache/shared_code/")

# set random seed for htmlwidget element id's (ensures stable widget ids across sessions)
htmlwidgets::setWidgetIdSeed(seed = 71607043)
```

#### DT German translation

Source: <https://datatables.net/plug-ins/i18n/German>

```{r}
dt_german <- list(
  sEmptyTable = "Keine Daten in der Tabelle vorhanden",
  sInfo = "_START_ bis _END_ von _TOTAL_ Einträgen",
  sInfoEmpty = "0 bis 0 von 0 Einträgen",
  sInfoFiltered = "(gefiltert von _MAX_ Einträgen)",
  sInfoPostFix = "",
  sInfoThousands = ".",
  sLengthMenu = "_MENU_ Einträge anzeigen",
  sLoadingRecords = "Wird geladen...",
  sProcessing = "Bitte warten...",
  sSearch = "Suchen",
  sZeroRecords = "Keine Einträge vorhanden.",
  oPaginate = list(sFirst = "Erste",
                   sPrevious = "Zurück",
                   sNext = "Nächste",
                   sLast = "Letzte"),
  oAria = list(sSortAscending = ": aktivieren, um Spalte aufsteigend zu sortieren",
               sSortDescending = ": aktivieren, um Spalte absteigend zu sortieren" ),
  select = list(rows = list(`_` = '%d Zeilen ausgewählt',
                            `0` = 'Zum Auswählen auf eine Zeile klicken',
                            `1` = '1 Zeile ausgewählt'))
)
```

#### Colors

```{r define_colors}
defacto_purple <- "#44187C"
defacto_yellow <- "#EDD02E"
```

### Shared functions

#### Vectorized version of `isTRUE()`

```{r is_true}
is_true <- Vectorize(FUN = isTRUE)
```

#### Get the mode(s) of a variable of any type

Solution borrowed from [hugovdberg](https://stackoverflow.com/a/38097776/7196903) and others.

```{r get_mode}
get_mode <- function(x,
                     method = "one",
                     na.rm = FALSE)
{
  x <- unlist(x)
  
  if ( na.rm ) {
    x <- x[!is.na(x)]
  }
  
  # get unique values
  ux <- unique(x)
  n <- length(ux)
  
  # get frequencies of all unique values
  frequencies <- tabulate(match(x, ux))
  modes <- frequencies == max(frequencies)
  
  # determine number of modes
  nmodes <- sum(modes)
  nmodes <- ifelse(nmodes == n, 0L, nmodes)
  
  if ( method %in% c("one", "mode", "") | is.na(method) )
  {
    # return NA if not exactly one mode, else return the mode
    if ( nmodes != 1 )
    {
      return(NA)
      
    } else return(ux[which(modes)])
    
  } else if ( method %in% c("n", "nmodes") )
  {
    # return the number of modes
    return(nmodes)
    
  } else if ( method %in% c("all", "modes") )
  {
    # return NA if no modes exist, else return all modes
    if ( nmodes > 0 )
    {
      return(ux[which(modes)])
      
    } else return(NA)
  }
  warning("Unrecognized parameter `method`. Valid methods are 'one'/'mode' (default), 'n'/'nmodes' and 'all'/'modes'")
}
```

#### Detach all non-basic packages

```{r detach_all_packages}
detach_all_packages <- function(verbose = FALSE)
{
  basic_packages <-  paste0("package:",
                            c("base",
                              "datasets",
                              "graphics",
                              "grDevices",
                              "methods",
                              "stats",
                              "utils"))
  
  loaded_packages <- grep(x = search(),
                          pattern = "package:",
                          value = TRUE,
                          perl = TRUE)
  
  to_detach <- setdiff(loaded_packages, basic_packages)
  
  if ( length(to_detach) > 0 )
  {
    for ( package in to_detach )
    {
      detach(package,
             character.only = TRUE,
             force = TRUE)
      
      if ( verbose ) message(package, " detached")
    }
  }
}
```

#### Load packages (install if necessary)

If a package isn't already installed, it will be tried to download and install it to the default package directory.

```{r ensure_package, message = FALSE}
ensure_package <- Vectorize(
  FUN =
    function(package,
             load = TRUE)
    {
      installed_packages <- rownames(installed.packages())
      
      if ( !(package %in% installed_packages) )
      {
        install.packages(package,
                         repos = "https://cloud.r-project.org/")
      }
      if ( load ) library(package, character.only = TRUE)
    }
)

# ensure the packages used in the subsequent code are installed
invisible(suppressPackageStartupMessages(
  ensure_package(package = c("checkmate",
                             "imager",
                             "dplyr",
                             "plotly",
                             "readr",
                             "RSelenium",
                             "rsvg"),
                 load = FALSE)
))
invisible(suppressPackageStartupMessages(
  ensure_package(package = "magrittr")
))
```

#### Get color of a specific Swiss party

```{r get_party_color}
get_party_color <- Vectorize(
  FUN =
    function(party,
             alpha = 255)
    {
      return(
        switch(
          EXPR = as.character(party),
          "AL" = rgb(238, 29, 35, alpha, maxColorValue = 255),
          "AL | PdA | solidaritéS" = rgb(238, 29, 35, alpha, maxColorValue = 255),
          "BDP" = rgb(255, 221, 0, alpha, maxColorValue = 255),
          "CSP" = rgb(0, 150, 167, alpha, maxColorValue = 255),
          "CSP Obwalden" = rgb(215, 45, 12, alpha, maxColorValue = 255),
          "CVP" = rgb(255, 133, 12, alpha, maxColorValue = 255),
          "EDU" = rgb(102, 51, 0, alpha, maxColorValue = 255),
          #"EDU" = rgb(0, 0, 0, alpha, maxColorValue = 255),
          "EVP" = rgb(204, 153, 255, alpha, maxColorValue = 255),
          #"EVP" = rgb(0, 97, 172, alpha, maxColorValue = 255),
          "FDP" = rgb(16, 79, 160, alpha, maxColorValue = 255),
          "glp" = rgb(195, 217, 63, alpha, maxColorValue = 255),
          "Grüne" = rgb(132, 180, 20, alpha, maxColorValue = 255),
          "Junge CVP" = rgb(255, 133, 12, alpha, maxColorValue = 255),
          "Junge glp" = rgb(195, 217, 63, alpha, maxColorValue = 255),
          "Junge Grüne" = rgb(132, 180, 20, alpha, maxColorValue = 255),
          #"Junge Grüne" = rgb(44, 79, 57, alpha, maxColorValue = 255),
          "Junge SVP" = rgb(63, 123, 23, alpha, maxColorValue = 255),
          #"Junge SVP" = rgb(0, 145, 61, alpha, maxColorValue = 255),
          "Jungfreisinnige" = rgb(16, 79, 160, alpha, maxColorValue = 255),
          #"Jungfreisinnige" = rgb(0, 76, 147, alpha, maxColorValue = 255),
          "JUSO" = rgb(238, 42, 59, alpha, maxColorValue = 255),
          #"JUSO" = rgb(238, 42, 46, alpha, maxColorValue = 255),
          "LDP" = rgb(0, 73, 144, alpha, maxColorValue = 255),
          "Lega" = rgb(102, 51, 0, alpha, maxColorValue = 255),
          "MCG" = rgb(249, 250, 0, alpha, maxColorValue = 255),
          "MCR" = rgb(249, 250, 0, alpha, maxColorValue = 255),
          "PdA" = rgb(255, 0, 0, alpha, maxColorValue = 255),
          "Piraten" = rgb(0, 0, 0, alpha, maxColorValue = 255),
          #"Piraten" = rgb(249, 178, 0, alpha, maxColorValue = 255),
          "SP" = rgb(238, 42, 59, alpha, maxColorValue = 255),
          "SVP" = rgb(63, 123, 23, alpha, maxColorValue = 255),
          paste0("#CCCCCC", as.hexmode(alpha)))
      )
    }
)
```

#### Auto-crop white space around PNG image

```{r autocrop_png}
autocrop_png <- function(path_to_png)
{
  imager::load.image(file = path_to_png) %>%
    imager::autocrop() %>%
    imager::pad(nPix = 4,
                axes = "xy",
                pos = 0) %>%
    imager::flatten.alpha() %>%
    imager::save.image(file = path_to_png)
}
```

#### Add enlargement link to plotly graph

```{r add_enlargement_annotation}
add_enlargement_annotation <- function(plotly_object,
                                       large_object,
                                       annotation_x = 0.8,
                                       annotation_y = -0.19,
                                       xanchor = "left",
                                       yanchor = "top")
{
  plotly_object %>%
    plotly::add_annotations(
      text = paste0("<a href='https://smartvote-ddj.gitlab.io/", large_object, ".html'>",
                    "vergrössern</a>"),
      xanchor = xanchor,
      yanchor = yanchor,
      showarrow = FALSE,
      font = list(family = "Work Sans",
                  size = 18,
                  color = "#44187C"),
      xref = "paper",
      yref = "paper",
      x = annotation_x,
      y = annotation_y
    )
}
```

#### Export htmlwidget as HTML file

```{r export_htmlwidget}
export_htmlwidget <- function(htmlwidget_object,
                              filename = NULL,
                              parent_path = paste0(getwd(), "/output/"),
                              selfcontained = FALSE,
                              libdir = NULL,
                              add_web_font = getOption("google_font.work_sans"))
{
  if ( is.null(filename) )
  {
    auto_name <- deparse(substitute(htmlwidget_object))
    
    filename <- dplyr::if_else(
      condition = auto_name == ".",
      true = "htmlwidget.html",
      false = paste0(deparse(substitute(htmlwidget_object)), ".html")
    )
  }
  
  if ( is.null(libdir) )
  {
    libdir <- paste0(tools::file_path_sans_ext(x = filename), "_files")
  }
  
  htmlwidgets::saveWidget(
    widget = htmlwidget_object,
    file = paste0(parent_path, filename),
    selfcontained = selfcontained,
    libdir = libdir
  )
  
  if ( !is.null(add_web_font) )
  {
    webfont_tag <-
      "<link href=\"" %>%
      paste0(checkmate::assert_character(x = add_web_font,
                                         pattern = "^https?://\\w.*",
                                         ignore.case = TRUE,
                                         any.missing = FALSE,
                                         all.missing = FALSE,
                                         unique = TRUE)) %>%
      paste0("\" rel=\"stylesheet\" />")
    
    readr::read_file(file = paste0(parent_path, filename)) %>%
      stringr::str_replace(pattern = "<link href=",
                           replacement = paste0(webfont_tag, "\n<link href=")) %>%
      readr::write_file(path = paste0(parent_path, filename),
                        append = FALSE)
  }
}
```

#### Export Plotly graph as interactive HTML widget

**Remember:**

- Plotly's default text color is `#444444`.
- Plotly's default grid color is `#808080`.

```{r export_plotly2HTML}
export_plotly2HTML <- function(plotly_graph,
                               filename = NULL,
                               parent_path = paste0(getwd(), "/output/"),
                               selfcontained = FALSE,
                               libdir = "plotly_files",
                               disable_legend_toggling = NULL,
                               add_web_font = getOption("google_font.work_sans"))
{
  if ( is.null(filename) )
  {
    auto_name <- deparse(substitute(plotly_graph))
    
    filename <- dplyr::if_else(
      condition = auto_name == ".",
      true = "plotly_graph.html",
      false = paste0(deparse(substitute(plotly_graph)), ".html")
    )
  }
  
  export_htmlwidget(htmlwidget_object = plotly_graph,
                    filename = filename,
                    parent_path = parent_path,
                    selfcontained = selfcontained,
                    libdir = libdir,
                    add_web_font = add_web_font)
  
  # optionally disable legend toggling of individual traces
  ## create necessary CSS rules
  if ( !is.null(disable_legend_toggling) )
  {
    test_char <- checkmate::check_choice(x = disable_legend_toggling,
                                         choices = "all")
    test_num <- checkmate::check_numeric(x = disable_legend_toggling,
                                         lower = 1,
                                         upper = length(plotly_graph$x$attrs),
                                         min.len = 1,
                                         max.len = length(plotly_graph$x$attrs),
                                         unique = TRUE,
                                         any.missing = FALSE,
                                         all.missing = FALSE)
    
    if ( !isTRUE(test_char) & !isTRUE(test_num) )
    {
      stop("Invalid argument provided: disable_legend_toggling\n",
           ifelse(!isTRUE(test_char) & is.character(disable_legend_toggling),
                  paste0(test_char, ". Or alternatively can also be a vector of integers >= 1 and <= number of traces."),
                  ""),
           ifelse(!isTRUE(test_num) & is.numeric(disable_legend_toggling),
                  paste0(test_num, ". Or alternatively can also be \"all\"."),
                  ""))
      
    } else if ( isTRUE(test_char) )
    {
      css_rules <-
        c("",
          "/* hides the svg dom element that has the click handler responsible for toggling */",
          ".legend .traces .legendtoggle {",
          "  display: none;",
          "}",
          "/* just for presentation: shows the default cursor instead of the text cursor */",
          ".legend .traces .legendtext {",
          "  cursor: default;",
          "}",
          "")
    } else
    {
      disable_legend_toggling %<>% as.integer()
      
      css_rules <-
        c("",
          "/* hides the svg dom element that has the click handler responsible for toggling */")
      
      for ( i in disable_legend_toggling )
      {
        css_rules %<>%
          c(paste0(".legend .groups:nth-of-type(", i, ") .legendtoggle",
                   dplyr::if_else(i == last(disable_legend_toggling),
                                  " {",
                                  ","), " "))
      }
      
      css_rules %<>%
        c("  display: none;",
          "}",
          "/* just for presentation: shows the default cursor instead of the text cursor */")
      
      for ( i in disable_legend_toggling )
      {
        css_rules %<>%
          c(paste0(".legend .groups:nth-of-type(", i, ") .legendtext",
                   dplyr::if_else(i == last(disable_legend_toggling),
                                  " {",
                                  ","), " "))
      }
      
      css_rules %<>%
        c("  cursor: default;",
          "}",
          "")
    }
    
    ## write new CSS rules to .css file
    ### get directory path of default .css file
    plotly_dir <-
      list.dirs(path = paste0(parent_path, libdir),
                full.names = TRUE,
                recursive = FALSE) %>%
      stringr::str_subset(pattern = "plotlyjs")
    
    ### create the folder `custom_dependencies` in the libdir if it doesn't already exist
    dir.create(path = paste0(dirname(path = plotly_dir), "/custom_dependencies"),
               showWarnings = FALSE)
    
    ### append CSS rules to a custom copy of the original file
    readr::read_lines(file = paste0(plotly_dir, "/plotly-htmlwidgets.css")) %>%
      c(css_rules) %>%
      readr::write_lines(path = paste0(dirname(path = plotly_dir), "/custom_dependencies/plotly-htmlwidgets_",
                                       tools::file_path_sans_ext(x = filename), ".css"),
                         append = FALSE)
    
    ## modify dependency path in HTML file to match the custom copy
    readr::read_file(file = paste0(parent_path, filename)) %>%
      stringr::str_replace(pattern = "(?<=<link href=\")([^\\/]+?\\/)[^\\/]+?\\/plotly-htmlwidgets\\.css",
                           replacement = paste0("\\1custom_dependencies/plotly-htmlwidgets_",
                                                tools::file_path_sans_ext(x = filename), ".css")) %>%
      readr::write_file(path = paste0(parent_path, filename),
                        append = FALSE)
  }
}
```

#### Export Plotly graph to SVG using `RSelenium`

**Remark:**

- The export is done using the automated testing framework [Selenium](https://de.wikipedia.org/wiki/Selenium) which results in opening a browser window (Google Chrome) that might has to be closed by hand.

- Other than Plotly's own `export()` function this one also allows to set the `width` and `height` of the exported plot (in the former it's hardcoded to 800x600 pixels).

- If `incl_PDF_copy`/`incl_PNG_copy` is set to `TRUE`, the exported SVG additionally gets converted to a PDF/PNG using the R package [`rsvg`](https://github.com/jeroen/rsvg/tree/40576ac326621b40224db344b09158f4ff717433) which relies on [`librsvg`](https://de.wikipedia.org/wiki/Librsvg). On Linux distributions the development package of `librsvg` must be installed. On macOS the required dependency (`librsvg`) can be installed using [Homebrew](https://brew.sh/).

```{r export_plotly2SVG}
export_plotly2SVG <- function(plotly_graph,
                              filename = NULL,
                              parent_path = paste0(getwd(), "/output/"),
                              width = 800,
                              height = 600,
                              remove_title = TRUE,
                              font_family = "Linux Biolinum O",
                              incl_PDF_copy = TRUE,
                              incl_PNG_copy = FALSE,
                              png_scaling_factor = 1.8,
                              autocrop_png = TRUE)
{
  if ( is.null(filename) )
  {
    auto_name <- deparse(substitute(plotly_graph))
    
    filename <- dplyr::if_else(
      condition = auto_name == ".",
      true = "plotly_graph.svg",
      false = paste0(deparse(substitute(plotly_graph)), ".svg")
    )
  } else
  {
    filename %<>%
      tools::file_path_sans_ext() %>%
      paste0(".svg")
  }
  
  # delete old SVG file
  if ( file.exists(paste0(parent_path, filename)) )
  {
    unlink(x = paste0(parent_path, filename))
  }
  
  if ( remove_title )
  {
    plotly_graph %<>%
      plotly::layout(title = "",
                     margin = list(t = 0))
  }
  
  if ( !is.null(font_family) )
  {
    plotly_graph %<>%
      plotly::layout(font = list(family = font_family))
  }
  
  # temporarily export plot to a HTML file
  tempfile <- tempfile(pattern = "plotly_temp_",
                       tmpdir = stringr::str_replace(string = parent_path,
                                                     pattern = "/$", 
                                                     replacement = ""),
                       fileext = ".html")
  
  export_plotly2HTML(plotly_graph = plotly_graph,
                     filename = basename(tempfile),
                     parent_path = parent_path)
  
  on.exit(unlink(tempfile),
          add = TRUE)
  
  # get <div> ID of exported htmlwidget
  htmlwidget_id <-
    stringr::str_extract(string = readr::read_file(file = tempfile),
                         pattern = "(?<=<div id=\")htmlwidget-[^\"]+")
  
  # initialize Chrome as RSelenium driver
  selenium_driver <-
    RSelenium::rsDriver(browser = "chrome",
                        extraCapabilities = list(
                          chromeOptions = list(
                            prefs = list(
                              "profile.default_content_settings.popups" = 0L,
                              "download.prompt_for_download" = FALSE,
                              "download.default_directory" = parent_path
                            )
                          )
                        ),
                        verbose = FALSE)
  
  # navigate to temporary HTML file
  selenium_driver$client$navigate(url = paste0("file://", normalizePath(tempfile)))
  
  # download plot as SVG using the native
  # [`Plotly.downloadImage`](https://plot.ly/javascript/plotlyjs-function-reference/#plotlydownloadimage) function
  selenium_driver$client$executeScript(
    script = paste0("Plotly.downloadImage(document.getElementById('", htmlwidget_id, "'), ",
                    "{format: 'svg', width: ", width, ", height: ", height, ", filename: '",
                    tools::file_path_sans_ext(x = filename), "'});"),
    args = list(NULL)
  )
  
  # wait for SVG to be saved to disk
  Sys.sleep(time = 1)
  
  # convert to PDF
  if ( incl_PDF_copy )
  {
    rsvg::rsvg_pdf(svg = paste0(parent_path, filename),
                   file = paste0(parent_path, stringr::str_replace(string = filename,
                                                                   pattern = "\\.svg$",
                                                                   replacement = ".pdf")))
  }
  
  # convert to PNG
  if ( incl_PNG_copy )
  {
    filename_png <- stringr::str_replace(string = filename,
                                         pattern = "\\.svg$",
                                         replacement = ".png")
    
    rsvg::rsvg_png(svg = paste0(parent_path, filename),
                   file = paste0(parent_path, filename_png),
                   width = png_scaling_factor * width,
                   height = png_scaling_factor * height)
    
    if ( autocrop_png ) autocrop_png(path_to_png = paste0(parent_path, filename_png))
  }
}
```

#### Export Plotly graph to PNG

```{r export_plotly2PNG}
export_plotly2PNG <- function(plotly_graph,
                              filename = NULL,
                              parent_path = paste0(getwd(), "/output/"),
                              width = 1072,
                              height = 824,
                              autocrop = TRUE,
                              remove_title = TRUE,
                              font_family = "Linux Biolinum O")
{
  if ( is.null(filename) )
  {
    auto_name <- deparse(substitute(plotly_graph))
    
    filename <- dplyr::if_else(
      condition = auto_name == ".",
      true = "plotly_graph.png",
      false = paste0(deparse(substitute(plotly_graph)), ".png")
    )
  }
  
  if ( remove_title )
  {
    plotly_graph %<>%
      plotly::layout(title = "",
                     margin = list(t = 0))
  }
  
  if ( !is.null(font_family) )
  {
    plotly_graph %<>%
      plotly::layout(font = list(family = font_family))
  }
  
  plotly_graph %>%
    plotly::export(file = paste0(parent_path, filename),
                   vwidth = width,
                   vheight = height)
  
  if ( autocrop ) autocrop_png(path_to_png = paste0(parent_path, filename))
}
```

#### Export DT table as interactive HTML widget

```{r export_DT2HTML}
export_DT2HTML <- function(dt_table,
                           filename = NULL,
                           parent_path = paste0(getwd(), "/output/"),
                           selfcontained = FALSE,
                           libdir = "dt_files")
{
  if ( is.null(filename) )
  {
    auto_name <- deparse(substitute(dt_table))
    
    filename <- dplyr::if_else(
      condition = auto_name == ".",
      true = "dt_table.html",
      false = paste0(deparse(substitute(dt_table)), ".html")
    )
  }
  
  htmlwidgets::saveWidget(
    widget = dt_table,
    file = paste0(parent_path, filename),
    selfcontained = selfcontained,
    libdir = libdir
  )
}
```

#### Fit Graded Response Model (Samejima 1969)

```{r fit_grm}
fit_grm <- function(data,
                    tolerance = 0.00001,
                    n_cycles = 10000)
{
  fitted_grm <-
    mirt(
      data = data %>% mutate_all(.funs = as.integer),
      model = 1,
      itemtype = "graded",
      # SE = TRUE,
      method = "EM",
      TOL = tolerance,
      technical = list(NCYCLES = n_cycles,
                       removeEmptyRows = TRUE)
    )
  
  return(fitted_grm)
}
```

#### Extract and assign IRT ability estimates (aka factor scores/latent trait estimates)

```{r assign_ability_estimates)}
assign_ability_estimates <- function(dataset,
                                     model,
                                     method = "MAP",
                                     invert = FALSE,
                                     new_var_name = NULL)
{
  # add temporary variable `index` for matching
  dataset %<>% rownames_to_column(var = "index")
  
  # detect dataset type
  dataset_type <- deparse(substitute(model))
  dataset_type <- case_when(
    str_detect(string = dataset_type, pattern = "smartvote_comparable_03_15") ~ "smartvote_comparable_03_15",
    str_detect(string = dataset_type, pattern = "smartvote_comparable_07_15") ~ "smartvote_comparable_07_15",
    str_detect(string = dataset_type, pattern = "smartvote_comparable_11_15") ~ "smartvote_comparable_11_15",
    str_detect(string = dataset_type, pattern = "smartvote") ~ "smartvote",
    str_detect(string = dataset_type, pattern = "selects_comparable_07_15") ~ "selects_comparable_07_15",
    str_detect(string = dataset_type, pattern = "selects") ~ "selects",
    str_detect(string = dataset_type, pattern = "narrow") ~ "parliament_special_narrow",
    str_detect(string = dataset_type, pattern = "special") ~ "parliament_special",
    str_detect(string = dataset_type, pattern = "all") ~ "parliament_all"
  )
  
  # read in smartvote and Selects variable mapping amd determine variable numbers if necessary
  if ( str_detect(string = dataset_type, pattern = "comparable") )
  {
    # detect dataset year
    data_year <- deparse(substitute(model)) %>% str_extract(pattern = "\\d{4}")
    
    # detect lower year of comparison
    lower_year_comparison <-
      deparse(substitute(model)) %>%
      str_extract(pattern = "\\d{2}") %>%
      as.integer() %>%
      add(2000)
    
    if ( str_detect(string = dataset_type, pattern = "smartvote") )
    {
      comparable_question_numbers <-
        read_csv(file = "data/smartvote_normal_question_mapping.csv",
                 col_types = "cicicicic",
                 na = c("", "-")) %>%
        bind_rows(read_csv(file = "data/smartvote_budgetary_question_mapping.csv",
                           col_types = "cicicicic",
                           na = c("", "-"))) %>%
        filter_(case_when(lower_year_comparison == 2003 ~
                            paste0("!is.na(variable_nr_2003) & !is.na(variable_nr_2007) ",
                                   "& !is.na(variable_nr_2011) & !is.na(variable_nr_2015)"),
                          lower_year_comparison == 2007 ~
                            paste0("!is.na(variable_nr_2007) & !is.na(variable_nr_2011) & !is.na(variable_nr_2015)"),
                          lower_year_comparison == 2011 ~
                            paste0("!is.na(variable_nr_2011) & !is.na(variable_nr_2015)"),
                          lower_year_comparison == 2015 ~
                            paste0("!is.na(variable_nr_2011) & !is.na(variable_nr_2015)"))) %>%
        .[[paste0("variable_nr_", data_year)]]
      
      # special case 2007: manually add 2nd public welfare budgetary question
      if ( data_year == 2007 & lower_year_comparison > 2003 ) comparable_question_numbers %<>% c(71)
      
    } else
    {
      comparable_question_numbers <-
        read_csv(file = "data/selects_question_mapping.csv",
                 col_types = "ciii",
                 na = c("", "-")) %>%
        filter(!is.na(variable_nr_2007_2011_en) & !is.na(variable_nr_2015_en)) %>%
        .[[if_else(data_year == 2015,
                   "variable_nr_2015_en",
                   "variable_nr_2007_2011_en")]]
    }
  }
  
  filter_regex <- switch(dataset_type,
                         "smartvote_comparable_03_15" = paste0("smartvote_question_(",
                                                               paste0(comparable_question_numbers, collapse = "|"),
                                                               ")_ordinal"),
                         "smartvote_comparable_07_15" = paste0("smartvote_question_(",
                                                               paste0(comparable_question_numbers, collapse = "|"),
                                                               ")_ordinal"),
                         "smartvote_comparable_11_15" = paste0("smartvote_question_(",
                                                               paste0(comparable_question_numbers, collapse = "|"),
                                                               ")_ordinal"),
                         "smartvote" = "smartvote_question_\\d+_ordinal",
                         "selects_comparable_07_15" = paste0("selects_question_english_(",
                                                             paste0(comparable_question_numbers, collapse = "|"),
                                                             ")_ordinal"),
                         "selects" = "selects_question_english_\\d+_ordinal",
                         # "number" is just a dummy value (no filtering necessary for parliament vote data)
                         "parliament_special_narrow" = "number",
                         "parliament_special" = "number",
                         "parliament_all" = "number")
  
  if ( is.null(new_var_name) )
  {
    new_var_name <- paste0("latent_lr_placement_", dataset_type)
  }
  
  ability_estimates <-
    fscores(object = model,
            full.scores.SE = TRUE,
            method = method) %>%
    as_data_frame() %>%
    rename(!!new_var_name := F1,
           !!paste0(new_var_name, "_se") := SE_F1) %>%
    mutate(index =
             dataset %>%
             filter_at(.vars = vars(matches(filter_regex)),
                       .vars_predicate = any_vars(!is.na(.))) %$%
             index)
  
  # invert left/right if necessary
  if ( invert ) ability_estimates[[new_var_name]] <- -ability_estimates[[new_var_name]]
  
  # remove possibly existing "old"" ability estimates
  dataset[[new_var_name]] <- NULL
  dataset[[paste0(new_var_name, "_se")]] <- NULL
  
  dataset %<>%
    left_join(y = ability_estimates,
              by = "index") %>%
    select(-index)
  
  return(dataset)
}
```
