## Short smartvote questionnaire variable overview

### Original variables

**year** | **variable naming scheme** | **scale of measurement** | **3-value** | **4-value**   | **5-value**
:------- | :------------------------- | :----------------------- | :---------: | :-----------: | :---------:
2003     | `ans_#`                    | ordinal                  | -           | 1–70          | -
2007     | `sv_question_#`            | pseudo-interval          | 64–73       | 1–63          | -
2011     | `C_sv_answer_#`            | pseudo-interval          | -           | 1–64          | 65–75
2015     | `answer_#`                 | pseudo-interval          | -           | 1433–1497     | 1498–1507

### Renamed variables

**year** | **variable naming scheme**          | **scale of measurement** | **3-value** | **4-value**   | **5-value**
:------- | :---------------------------------- | :----------------------- | :---------: | :-----------: | :---------:
2003     | `smartvote_question_#_ordinal`      | ordinal                  | -           | 1-70          | -
2007     | `smartvote_question_#_interval`     | pseudo-interval          | 64–73       | 1–63          | -
2011     | `smartvote_question_#_interval`     | pseudo-interval          | -           | 1–64          | 65–75
2015     | `smartvote_question_#_interval`     | pseudo-interval          | -           | 1–65          | 66–75

